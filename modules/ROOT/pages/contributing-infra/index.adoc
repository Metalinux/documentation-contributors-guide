include::ROOT:partial$attributes.adoc[]

= Contribute to keeping Docs up an running

[abstract]
____
This document explains how to contribute to maintaining and improving the publishing system used to build the Fedora Documentation website.
____

Maintainig the Docs website is a lot of work needing skills in different areas.

Graphical design::
The graphical and functional appearance of Docs web pages require continuous adaptation to additional or changing needs. Members with expertise in web design, UI or UX development can help optimize these areas.  
Git maintainers::
All documentation is stored in GIT along with issues and organisational tasks. Community members with experience in managing GIT repositories can help maintain and update the Docs repositories. 
Workflow maintenance::
The processes for creating the web pages are automated. Members with experience in CI can contribute to maintain and improve these processes. 

The relevant sections provide further information. 